const mosca = require('mosca')
const server = new mosca.Server()

import { boasVindas } from './boas-vindas.js'
import { recuperaTopicos, atualizaEstado } from './topicos.js'
import { topicosAtualizados } from './helpers.js'

console.log(boasVindas)

server.on('ready', () => console.log('Olá, servidor de pé!'))

server.on('clientConnected', (client) => console.log(`Novo dispositivo conectado: ${client.id}`))

server.on('published', (packet) => {
  const { topic, payload } = packet
  const status = payload.toString()

  console.log('topico', topic)
  console.log('status', status)
  console.log('payload', payload)
  return

  if (topico && status == 'OFF' || status == 'ON') {
    const novoStatus = [{
      'nome': topico,
      'status': status
    }]

    atualizaEstado( topicosAtualizados( recuperaTopicos(), novoStatus ) )
    console.log( 'Solicitação recebida, seu novo status é' );
    console.log( recuperaTopicos() )
  }
})
