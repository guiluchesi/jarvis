const topicosIguais = (topicoAtual, topicoRecebido) =>
  topicoAtual.nome === topicoRecebido.nome

const atualizaTopico = (topicoAtual, topicoRecebido) =>
  topicosIguais(topicoRecebido, topicoAtual) ? topicoRecebido : topicoAtual

export const topicosAtualizados = (topicos, statusRecebido) =>
  topicos.map(topico => statusRecebido.reduce(atualizaTopico, topico))
