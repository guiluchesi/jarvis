const chalk = require('chalk')

export const boasVindas = '' +
  '\n              ' + chalk.bold.gray('`:XXXXXX/-`') + '\n' +
  '            ' + chalk.bold.gray('.XXXX/`') + '\n' +
  '           ' + chalk.bold.gray('/XXX./XX.') + '                    /$$$$$  /$$$$$$  /$$$$$$$  /$$    /$$ /$$$$$$  /$$$$$$ \n' +
  '          ' + chalk.bold.gray('/XXX. XXXX-') + '                  |__  $$ /$$__  $$| $$__  $$| $$   | $$|_  $$_/ /$$__  $$\n' +
  '          ' + chalk.bold.gray('XXXXXX.XXXX`') + '                    | $$| $$    $$| $$    $$| $$   | $$  | $$  | $$  \\__/\n' +
  chalk.bold.gray('`        :XXXXXX.`XXX-X+.') + '                 | $$| $$    $$| $$    $$| $$   | $$  | $$  | $$  \\__/\n' +
  chalk.bold.gray('+`     .XXXXXX/- .XXX.XXXX`') + '               | $$| $$$$$$$$| $$$$$$$/|  $$ / $$/  | $$  |  $$$$$$ \n' +
  chalk.bold.gray('.X-   .XXXX/.:X+. -++ .XXXX`') + '         /$$  | $$| $$__  $$| $$__  $$ \\  $$ $$/   | $$   \\____  $$\n' +
  ' ' + chalk.bold.gray('.XX:.XXX+` -XXXXXX:---.:XXX') + '        | $$  | $$| $$  | $$| $$  |  $$ \\  $$$/    | $$   /$$  \\ $$\n' +
  '  ' + chalk.bold.gray('`+XXXXX+XXXXXXXXXXXXXX./XX`') + '       |  $$$$$$/| $$  | $$| $$   | $$  \\  $/    /$$$$$$|  $$$$$$/\n' +
  '    ' + chalk.bold.gray('`./XXXXXXXX:.-/XXXX+-.XX') + '         \\______/ |__/  |__/|__/   |__/   \\_/    |______/ \\______/ \n' +
  '          ' + chalk.bold.gray('``             /X/') + '\n' +
  '                        ' + chalk.bold.gray('-X:') + '\n'
