const fs = require('fs');
const arquivoDeTopicos = process.cwd() + '/topicos.txt'

export const recuperaTopicos = () =>
  JSON.parse( fs.readFileSync(arquivoDeTopicos).toString() );

export const atualizaEstado = novoEstado =>
  fs.writeFileSync(arquivoDeTopicos, JSON.stringify(novoEstado))
